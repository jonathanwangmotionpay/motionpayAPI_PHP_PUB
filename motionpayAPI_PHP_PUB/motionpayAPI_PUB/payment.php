<?php
ini_set('date.timezone', 'Asia/Shanghai');
require_once "./lib/MotionPay.Config.php";
require_once "./lib/MotionPay.Api.php";
require_once "./lib/Log.php";

$logHandler = new CLogFileHandler(MotionPayConfig::getMotionPayLogFilename());
$log = Log::Init($logHandler, 15);

header("Content-Type:text/html;charset=utf-8");
/**
 * Work flow:
 * 1. Send the QRcode request to get the payment URL, Generate QRCode Image.
 * 2. Use Wechat or Alipay smart phone to scan the image and finish the payment.
 * 3. When the payment is successfully done, the server will send notify to the URL (callback.php) passed in.
 * 4. Use JavaScript to check the return value from paymentNotify.php to see if we got the callback notification in the log file.
 */
/**
 * 流程：
 * 1、创建QRCode支付单，取得code_url，生成二维码
 * 2、用户扫描二维码，进行支付
 * 3、支付完成之后，MotionPay服务器会通知支付成功（见：callback.php）
 * 4、在支付成功通知中需要查单确认是否真正支付成功（见：notify.php）
 */

$totalFee = "1";
$paymentType = "H5_A";
$scanImageHtmlCode = "<img src='images/wechatpay.png'/>";
$h5_payment = false;
$currentURL = "";
$mid = "";
$input = new MotionPayOrder();


if(isset($_POST['currentURL'])) {
    $currentURL = $_POST['currentURL'];
    // echo "currentURL2 is:" . $currentURL;
    if(strlen($currentURL) > 0) {
        // echo "currentURL is:" . $currentURL;
        MotionPayConfig::setDemoServerURL($currentURL);
    }
}
if(isset($_POST['paymentAmount'])) {
    $paymentAmountInReq = $_POST['paymentAmount'];
    $totalFee = strval($paymentAmountInReq * 100);
}

if(isset($_POST['paymentType'])) {
    $paymentType = $_POST['paymentType'];
}
if($paymentType == "W") {
    $input->setMerchantType(MotionPayConfig::ONLINE_MERCHANT);
    $scanImageHtmlCode = "<img src='images/wechatpay.png'/>";
}
else if($paymentType == "A") {
    $input->setMerchantType(MotionPayConfig::ONLINE_MERCHANT);
    $scanImageHtmlCode = "<img src='images/alipay.png'/>";
}
else if($paymentType == "H5_A") {
    $h5_payment = true;
    $input->setMerchantType(MotionPayConfig::H5_MERCHANT);
    $scanImageHtmlCode = "<img src='images/alipayH5.png'/>";
}
else if($paymentType == "H5_W") {
    $h5_payment = true;
    $input->setMerchantType(MotionPayConfig::H5_MERCHANT);
    $scanImageHtmlCode = "<img src='images/wechatH5.png'/>";
}
else {
    $errorMsg = "We can only support Wechat and Alipay for now.";
}

$orderId = MotionPayApi::getNonceStr(10);
$outTradeNo = date("YmdHis") . $orderId;
$input->setOutTradeNo($outTradeNo);
// $input->setOutTradeNo("201712142891735269456");
$input->setGoodsInfo("Test_Product");
$input->setTotalFee($totalFee);
$input->setTerminalNo("WebServer");
$input->setSpbillCreateIP();

$log->INFO("paymentType is:" . $paymentType);
$input->setPayChannel($paymentType);
$input->setNotifyUrl(MotionPayConfig::getCallbackURL());
$input->setMid();
$mid = $input->getMid();


$url2 = "";
$message = "";
if($h5_payment == true) {
    $wap_URL = MotionPayConfig::getWapURL() . "?orderId=" . $outTradeNo;
    // echo "wap url is:" . $wap_URL . "\n";
    $input->setWapURL($wap_URL);
    $result = MotionPayApi::H5Order($input);
    if ($result['code'] == '0') {
        $content = $result['content'];
    }
    else {
        $message = $result['message'];
    }
    $inputForSign = new MotionPayDataBase();
    $inputForSign->setMerchantType(MotionPayConfig::H5_MERCHANT);
    $inputForSign->setOutTradeNo($outTradeNo);
    $inputForSign->setMid();
    $mid = $inputForSign->getMid();
    $signReq = $inputForSign->makeSign();
    $url2 = MotionPayConfig::getURL(MotionPayConfig::GET_PAY_URL) . "?mid=" . $mid . "&out_trade_no=" . $outTradeNo . "&sign=" . $signReq;
    
    $_SESSION["orderId"] = $orderId;
    $_SESSION["paymentAmount"] = $totalFee;
    $_SESSION["mid"] = $mid;
    
    // echo "url2 is:" . $url2 . "\n";
}
else {
    $result = MotionPayApi::qrOrder($input);
    if ($result['code'] == '0') {
        $content = $result['content'];
        $url2 = $content['qrcode'];
    }
    else {
        $message = $result['message'];
    }
}
// echo "url2 is:" . $url2;
$log->INFO("mid in payment.php is:" . $mid);
$orderinfo = array($mid, $outTradeNo, $totalFee);
$log->writeOrderToCSVFile(MotionPayConfig::getMotionPayOrderCSVFilename(), $orderinfo);
// echo "the file name is:" . MotionPayConfig::getMotionPayOrderCSVFilename();
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" " http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 
  <head>
  <title>Motion Pay Sample Payment Page</title>

	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>  
	<meta charset="UTF-8"/>  
	<link href="web.css" rel="stylesheet" />
	<link rel="icon" href="images/MotionpayIcon.gif" type="image/gif" sizes="16x16"/>
	
<script>
var myTimeoutVar;
var counterForTimeout = 0;

function checkPaymentResult() {
	var paid = false;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "paymentNotify.php?mid=<?php echo $mid ?>&orderId=<?php echo $outTradeNo ?>&paymentAmount=<?php echo $totalFee ?>&", true);
	xhr.onload = function (e) {
	  if (xhr.readyState === 4) {
	    if (xhr.status === 200) {
	      var resultStr = xhr.responseText;
	      console.log(resultStr);
	      resultStr = resultStr.trim();
	      // alert("resultStr is:#" + resultStr +"#");
	      if(resultStr == "paid") {
	    	  // alert("payment is done.");
	    	  paid = true;
	    	  var divToUpdate = document.getElementById("infor_box");
	    	  divToUpdate.innerHTML = "<br/><font class='cOrange' style='font-size: 25px;'>Thank you very much for your payment.</font>";
	    	  clearTimeout(myTimeoutVar);
	      }
	    } else {
	      console.error(xhr.statusText);
	    }
	  }
	};
	xhr.onerror = function (e) {
	  console.error(xhr.statusText);
	};
	if(paid == false) {
		xhr.send(null);
		counterForTimeout = counterForTimeout + 1;
		// after 2 hours the QR code will be expired. Then we needn't to check if the order has been paid or not any more.
		// 两个小时后，QR码会过期，我们就不在需要检查订单是否已经支付了。
		// if(counterForTimeout < 5) { // for testing.
		if(counterForTimeout < 2*60*60/5) {
			// we check the order is paid or not every 5 seconds here. You can check every 1 second if you need to. Less than 1 seond will be rejected.
			// 我们每五秒钟查询一次订单是否已经支付。如果需要的话，您可以每1秒钟查询一次，间隔小于一秒钟的频发查询会返回错误。
			myTimeoutVar = setTimeout(checkPaymentResult, 5000);
		}
		else {
			var divToUpdate = document.getElementById("infor_box");
			// You can redirect the user to other pages if you like. Just change the href target here.
			// 您也可以重定向到其他页面，修改这里href的值就可以了。
			divToUpdate.innerHTML = "<br/><a href='JavaScript: window.history.go(-1);'><font class='cOrange' style='font-size: 25px;'>QR code is expired. Please click here to order again.</font></a>";
			clearTimeout(myTimeoutVar);
		}
	}
}


function setTimeoutCheckFunc() {
	myTimeoutVar = setTimeout(checkPaymentResult, 1000);
}
setTimeoutCheckFunc();
</script>		
</head>
<body>
<!-- header -->
<div id="header">
  <div class="logo">
    <a href="#" class="logoImg logoPic" style="cursor: default;"></a>
  </div>
  <a href="#" class="aProblem">Tech Support</a>
</div>
<!--header-->

<!--content-->
<div class="width1003" >
  <div class="pay_infor"  >
  
    <!--  ##<?php echo $currentURL; ?>##  -->
    <p><font class="cOrange" style="font-size: 25px;">Order id:<?php echo $orderId ?></font></p>
    <br/>
<?php if($h5_payment == true) {
    if(strlen($url2) > 0) {
        header("Location: " . $url2);
        die();
    }
    else {
        echo "H5 Redirect is failed. Message:" . $message . "\n";
    }
} else { ?>
    <div id="infor_box" class="infor_box" style="height:320px;">
    	<p><font style="font-size:15px;font-weight:bold;color:#2489c4;">
	 		Please scan this image from you cell phone to pay it:</font></p>
	 	 <br/>
	 	 <?php echo $scanImageHtmlCode; ?> <br/>
		<img alt="Mobile Scan" src="qrcode.php?data=<?php echo urlencode($url2); ?>" style="width:220px;height:220px;"/>
    </div>
<?php } ?>    
  </div>
</div>
</body>
</html>